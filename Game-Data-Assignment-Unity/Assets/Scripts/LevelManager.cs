﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

    public Transform groundHolder;
    public Transform turretHolder;
    public Transform cactusHolder;

    public enum MapDataFormat {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject cactusPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> cactusPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
   //This section clears out the main console. It gets the log in the editor, clears them. And resets the editor for upcoming log calls. 
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }


    // This function destroys all the children of a given parent. 
    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel() {

        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
        DestroyChildren(cactusHolder);

        // First part, combinds the streaming assets folder path with a string "maps" to find the maps folder. 
        // The second part, uses that folder and combines the file name into it. Storing it in the variable tmxfile. 
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // If there is a map you clear it.
        // Creating a string reader that is being passed the content string which contains the TMX files as a string. 
        //Next section, looks for a specific word in the string. Of that section it converts the string attribute into an int and assigns it to a variable.
        {
            mapData.Clear();

            string content = File.ReadAllText(TMXFile);

            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;


                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));


                reader.ReadToFollowing("layer");


                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");
                //This changes the encoding type from base64 or csv. Depending on the string you get the from the "data" it parses it differently"

                switch (encodingType) {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }
                //Trims the string splitting into sections
                mapDataString = reader.ReadElementContentAsString().Trim();

                //deleteing every position that was previously stored in the turrets position list
                turretPositions.Clear();
                cactusPositions.Clear();

                if (reader.ReadToFollowing("objectgroup")) {

                    if (reader.GetAttribute("name") == "Enemies")
                    {
                        if (reader.ReadToDescendant("object"))
                        {
                            do
                            {
                                //finding positions of objects taking into account pixelsperunit scale and loops it through all the objects. 
                                float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                                float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                                turretPositions.Add(new Vector3(x, -y, 0));

                            } while (reader.ReadToNextSibling("object"));
                        }
                    }
                }

                if (reader.ReadToFollowing("objectgroup"))
                {
                    if (reader.GetAttribute("name") == "Cactus")
                    {
                        if (reader.ReadToDescendant("object"))
                        {
                            do
                            {
                                //finding positions of objects taking into account pixelsperunit scale and loops it through all the objects. 
                                float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                                float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                                cactusPositions.Add(new Vector3(x, -y, 0));

                            } while (reader.ReadToNextSibling("object"));
                        }
                    }
                }

            }


            switch (mapDataFormat) {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while (index < bytes.Length) {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines) {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values) {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }

        //calculates the tile width and height based on the pixels per unit scale. Does the same thing for the offset. 
        {

            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // Instantiating texture by reading the bytes in the sprite sheet file and loading them into a texture. 
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // Clears the previous map sprites.  
        {
            mapSprites.Clear();
            //Cutting the sprites into sprite tiles by looping through rows and columns. 
            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        // Clears the previous tiles. 
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {
                    //Loops through the columns and rows of the map fetching the tile id and map data index. 
                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    //Instantiates a tile based on a tile prefab. Taking into account x,y pos and all the properties required. 
                    //Gets the sprite renderer of each child and assigns the proper sprite. Parents under the ground holder and adds to the tile list. 
                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        // For each turret position inside the list of turret positions, creates a turret prefab on the turret position using map center offset. 
        //Names the turret to be turret and parents it
        {
            foreach (Vector3 turretPosition in turretPositions) {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }

            foreach (Vector3 cactusPosition in cactusPositions)
            {
                GameObject cactus = Instantiate(cactusPrefab, cactusPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                cactus.name = "Cactus";
                cactus.transform.parent = cactusHolder;
            }
        }

        //Gets the time of loading and prints it. 
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


