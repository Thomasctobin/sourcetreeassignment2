﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileText : MonoBehaviour
{
    public bool canRotate = true;
    // Use this for initialization
    void Start()
    {
        InvokeRepeating("firstRotate", 0f, 4f);
        InvokeRepeating("secondRotate", 2f, 4f);
    }

    // Update is called once per frame
    void Update()
    {
      

    }



    void firstRotate()
    {
       // canRotate = false;
        this.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 5));
        this.GetComponent<TextMesh>().color = new Color(255, 0, 0);
        this.GetComponent<TextMesh>().fontStyle = FontStyle.Bold;
        //Rotate here
    }
    void secondRotate()
    {
        // yield return new WaitForSeconds(2);
        this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -5));
        this.transform.localScale = new Vector3(.5f, .5f, .5f);
        this.GetComponent<TextMesh>().color = new Color(0, 255, 0);
        this.GetComponent<TextMesh>().fontStyle = FontStyle.Italic;
        //canRotate = true;
    }

    // yield return new WaitForSeconds(2);

}



