﻿using System;
using UnityEngine;

[Serializable]
public class TextSave : Save
{
    public Data data;
    private TileText tileText;
    private string jsonString;


    //In between loads and saves this is where the data gets stored.
    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Color spriteColor;
        public FontStyle fontthickness;
        public string text;
        public bool canrepeat;

    }

    // Gets the tank object and inits the data. 
    void Awake()
    {
        tileText = GetComponent<TileText>();

        data = new Data();
    }

    // Getting value from the objects, and saving into an instance of data and using it to write a json file. 
    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.spriteColor = tileText.GetComponent<TextMesh>().color;
        data.fontthickness = tileText.GetComponent<TextMesh>().fontStyle;
        data.position = tileText.transform.position;
        data.eulerAngles = tileText.transform.eulerAngles;
        data.text = tileText.GetComponent<TextMesh>().text;
        //  data.canrepeat = tileText.canRotate;
        jsonString = JsonUtility.ToJson(data);

        return (jsonString);
    }
    //Reading from the json file, parsing the data and applying to the object. 
    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tileText.GetComponent<TextMesh>().color = data.spriteColor;
        tileText.transform.position = data.position;
        tileText.transform.eulerAngles = data.eulerAngles;
        tileText.GetComponent<TextMesh>().text = data.text;
        tileText.GetComponent<TextMesh>().fontStyle = data.fontthickness;
        // tileText.canRotate = data.canrepeat;
        tileText.name = "TileText";


    }
}