﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour
{

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    // This is a public static reference to the current instance of game save manager. This allows methods to be called staticly from other scripts. 
    public static GameSaveManager Instance { get; private set; }


    //Subscribing/unsubscribes to the event sceneLoaded. 
    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        //Getting the data path file, combining the persistent data path with game data file name. Init a list named saves items. . 
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }

    //Adds item to the saved items list. 
    public void AddObject(string item)
    {
        saveItems.Add(item);
    }

    //Saves the current data into a json. 
    public void Save()
    {
        saveItems.Clear();
        //Gets a list of all saveable objects in the scene. And for each one of them adds the json string info into the saves item list. 
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects)
        {
            saveItems.Add(saveableObject.Serialize());
        }
        // For each item in save items, calls the stream writing to write data into a json file. By taking the game data file path.  
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath))
        {
            foreach (string item in saveItems)
            {
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    //Prevents first play from breaking by setting first play to false. Clears the current save items and loads a new scene. 
    public void Load()
    {
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //whenever a scene is loaded, if this is the first play returns.  Otherwise loads the data, destorys all the objects currently in the scene and creates new game objects. 
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (firstPlay) return;

        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }

    //Using the stream reader to read the game data file, and while there is data, trims the lines and stores it into the save items list. 
    void LoadSaveGameData()
    {
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath))
        {
            while (gameDataFileStream.Peek() >= 0)
            {
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0)
                {
                    saveItems.Add(line);
                }
            }
        }
    }
    //Gets all saveable objects and for each one destroys them.
    void DestroyAllSaveableObjectsInScene()
    {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects)
        {
            Destroy(saveableObject.gameObject);
        }
    }
    //For each item in the save item list, instantiates a game object based on a prefab, and this deserializes its data into the objects. 
    void CreateGameObjects()
    {
        foreach (string saveItem in saveItems)
        {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
