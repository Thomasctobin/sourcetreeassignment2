﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save
{

    public Data data;
    private Tank tank;
    private string jsonString;

    //In between loads and saves this is where the data gets stored.
    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    // Gets the tank object and inits the data. 
    void Awake()
    {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    // Getting value from the objects, and saving into an instance of data and using it to write a json file. 
    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }
    //Reading from the json file, parsing the data and applying to the object. 
    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}